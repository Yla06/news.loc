$(document).ready(function () {

    $(document).on('click', '.close-bloc-image', function () {
        $(this).parents('li').detach();
    });

});

$(document).on('change', '#tags', function (e) {
let self = this;
    $.ajax({
        url: '/admin/tags/has-tag-in-news/' + $(this).val(),
        type: 'get',
        dataType: 'html',
        success: function (data) {
            $('.tag-news-error').remove();
            let div = $(document.createElement('div')).addClass('tag-news-error')
                .html(data)

            $(self).after(div)
        }
    });

});


function confirmDelete($msg) {

    var result = confirm($msg);

    if (result) {
        return true;
    } else {
        return false;
    }

}
