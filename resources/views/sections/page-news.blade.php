<div class="container-fluid post">
        <div class="row">
            <div class="col-6 offset-md-3 text-center">
                <h2>{{$post->title}}</h2>
            </div>
        </div>
            <div class="col-6 offset-md-3 text-center">
                <img src="{{asset($post->main_img)}}" class="img-post img-responsive" alt="{{$post->title}}">
                <div class="row">
                    <div class="col-4 text-left">
                        @if($post->views > 0)
                            <i class="fa fa-eye" aria-hidden="false"></i><span class="text-right pl-2">{{$post->views}}</span>
                        @endif
                    </div>
                    <div class="col-4 text-center">
                        <p id="star">
                            <i class="fa fa-star {{$post->star >= 1 ? 'text-warning' : 'text-secondary'}}" aria-hidden="true" data-num="1"></i>
                            <i class="fa fa-star {{$post->star >= 2 ? 'text-warning' : 'text-secondary'}}" aria-hidden="true" data-num="2"></i>
                            <i class="fa fa-star {{$post->star >= 3 ? 'text-warning' : 'text-secondary'}}" aria-hidden="true" data-num="3"></i>
                            <i class="fa fa-star {{$post->star >= 4 ? 'text-warning' : 'text-secondary'}}" aria-hidden="true" data-num="4"></i>
                            <i class="fa fa-star {{$post->star >=5  ? 'text-warning' : 'text-secondary'}}" aria-hidden="true" data-num="5"></i>
                        </p>
                    </div>
                    <div class="col-4">
                        <p class="news-date text-right">{{$post->created_at->format('d.m.Y')}}</p>
                    </div>
                </div>
            </div>

            <div class="col-6 offset-md-3">
                <p>{!!$post->body!!}</p>
            </div>
        </div>


        <div class="row">
            <div class="col-6 offset-md-3" style="padding-left: 40px;">
                <div class="row">
                    <h4 class="text-left p-2">Напишіть коментар</h4>
                </div>
                <form method="post" action="{{route('add-coment',$post->id)}}" >
                    <div class="form-group row">
                        <label for="name"  class="col-sm-2 col-form-label">Ім'я</label>
                        <div class="col-sm-10">
                            <input id="name" type="text" name="name" class="form-control @error('name') is-invalid @enderror">
                            @error('name')
                            <div class="alert text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2 col-form-label">Прізвище</label>
                        <div class="col-sm-10">
                            <input type="text"  id="surname" name="surname" class="form-control @error('surname') is-invalid @enderror">
                            @error('surname')
                            <div class="alert text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="secondname" class="col-sm-2 col-form-label">По-батькові</label>
                        <div class="col-sm-10">
                            <input type="text" id="secondname" name="secondname" class="form-control @error('secondname') is-invalid @enderror">
                            @error('secondname')
                            <div class="alert text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row" >
                        <label for="phone" class="col-sm-2 col-form-label">Телефон</label>
                        <div class="col-sm-10">
                            <input type="number" id="phone" name="phone" class="form-control @error('phone') is-invalid @enderror"  >
                            @error('phone')
                            <div class="alert text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="coment" class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10">
                            <textarea class="form-control @error('coment') is-invalid @enderror" name="coment" placeholder="Напишіть коментар..." rows="6" id="coment"></textarea>
                            @error('coment')
                            <div class="alert text-danger">{{ $message }}</div>
                            @enderror

                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" /><br />
                            <button type="submit" class="btn btn-primary">Надіслати</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-6 offset-md-3">
                <h4 style="margin-top: 20px;">Коментарі</h4>
                @foreach($comments as $comment)
                    @if($comment->status == 2)
                        <p>{{$comment->coment}}</p>
                    @endif
                @endforeach
            </div>
        </div>

</div>
<script>
        $('#star i').click(function (el) {

            $.ajax({
                url: '/up-star/{{$post->id}}/' + $(this).data('num'),
                type: 'get',
                success: function () {
                    location.reload();
                }
            })
        });
</script>



