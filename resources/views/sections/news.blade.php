<div class="container-fluid">
    <div class="news-block">
        <div class="row no-gutters">
            @foreach($news as $post)
                <div class="col-md-6">
                    <div class="news-body news-image" >
                        <img src="{{asset($post->main_img)}}" class="center-block img-responsive" alt="{{$post->title}}"  >
                    </div>
                    <div class="news-body">
                        <h5 class="title-news">
                            <a href="{{route('news-page', ['id' => $post->slug . '-' . $post->id])}}" >{{$post->title}}</a></h5>
                        <p class="card-text">{!!$post->short_body!!}</p>
                        <div class="row no-gutters">
                            <div class="col-sm-6 text-right col-md-6">
                                <p class="news-date" content="{{$post->created_at->format('d.m.Y')}}">{{$post->created_at->format('d.m.Y')}}</p>

                                @if($post->views > 0)

                                    <p>Перегляди: {{$post->views}}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
        <div class="row paginate no-gutters">
            {{ $news->links() }}
        </div>
    </div>
</div>

