@extends('layouts.base')
@section('content')
    @include('sections.header')
    @include('sections.news')
    @include('sections.footer')
@endsection
