<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('news_id')->index()->comment('Id новини');
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
            $table->string('name')->comment('Ім\'я');
            $table->string('surname')->nullable()->comment('Прізвище');
            $table->string('secondname')->nullable()->comment('По-батькові');
            $table->string('phone')->nullable()->comment('Телефон');
            $table->text('coment')->comment('Коментар');
            $table->string('status')->default(0)->comment('0-не опублікований, 1-на модерації, 2-опублікований');
            $table->integer('parent_id')->default(0)->comment('Id коменатаря');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coments');
    }
}
