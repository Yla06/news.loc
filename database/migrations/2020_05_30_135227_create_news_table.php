<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('title')-> comment('Назва новини');
            $table->string('main_img')->nullable()->comment('Фото');
            $table->text('short_body')->nullable()->comment('Короткий опис');
            $table->text('body')->nullable()->comment('Текст новини');
            $table->string('slug')->comment('URL');
            $table->boolean('visible')->default(0)->comment('Видимість на сайті: 0-не відображати, 1-відображати');
            $table->text('tags')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
