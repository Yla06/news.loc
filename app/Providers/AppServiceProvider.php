<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;
use App\Providers\UniqueTagValidator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::resolver(function($translator, $data, $rules, $messages){
            return new UniqueTagValidator($translator, $data, $rules, $messages);
        });
    }
}
