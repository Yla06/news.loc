<?php
namespace App\Providers;
use App\Http\Node\Model\TagsNodeModel;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Validator;

class UniqueTagValidator extends Validator {

    public function validateUniqueTag ($attribute, $value, $parameters)
    {

        $value = explode(',', Request::input('tags'));

       foreach($value as &$val) {
           $val = trim($val);
       }
        unset($val);

        $tags = TagsNodeModel::select('tags.id')->join('news_tags', 'news_tags.tags_id', '=', 'tags.id');

        if( !empty($parameters[0]))
            $tags = $tags->where('news_tags.news_id', '!=', $parameters[0]);

        $count = $tags->whereIn('tags.name', $value)->count();

        if( $count > 0) {
            return false;
        }
        return true;
    }
}
