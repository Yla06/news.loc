<?php

namespace App\Providers;

use Trafik8787\LaraCrud\LaraCrudProvider as ServiceProvider;

class LaraCrudProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */

    protected $navigation = [


        'App\Http\Node\NewsNode' => [
            'priory' => 2,
            'title' => 'Новини',
            'icon' => 'fa-tree'
        ],

         'App\Http\Node\CommentNode' => [
            'priory' => 3,
            'title' => 'Коментарі',
            'icon' => 'fa-tree'
        ]
    ];

    protected $nodes = [
        'App\Http\Node\Model\NewsNodeModel' => 'App\Http\Node\NewsNode',
        'App\Http\Node\Model\CommentNodeModel' => 'App\Http\Node\CommentNode',
        ];

    public function boot()
    {

    }

}
