<?php

function snake_case($s)
{
    return \Illuminate\Support\Str::snake($s);
}

function str_plural($s)
{
    return \Illuminate\Support\Str::plural($s);
}

//function str_contains($haystack, $needles)
//{
//    return \Illuminate\Support\Str::contains($haystack, $needles);
//}
function str_slug($s)
{
    return \Illuminate\Support\Str::slug($s, '-');
}
