<?php

namespace App\Http\Node\Model;

use Illuminate\Database\Eloquent\Model;

class CommentNodeModel extends Model
{
    protected $table = 'comments';

    public function news() {
        return $this->belongsTo(NewsNodeModel::class);
    }
}
