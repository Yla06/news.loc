<?php

namespace App\Http\Node\Model;

use Illuminate\Database\Eloquent\Model;

class TagsNodeModel extends Model
{
    protected $table = 'tags';

    protected $fillable = ['id', 'name', 'slug', 'created_at', 'updated_at'];
}
