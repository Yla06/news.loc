<?php

namespace App\Http\Node\Model;

use Illuminate\Database\Eloquent\Model;

class NewsNodeModel extends Model
{
    protected $table = 'news';

    public function tags() {
        return $this->belongsToMany(TagsNodeModel::class, 'news_tags', 'news_id', 'tags_id');
    }

    public function coments() {
        return $this->hasMany('App\Http\Node\Model\CommentNodeModel','news_id', 'id');
    }

    public function getUrl() {

        return $this->slug.'-'.$this->id;

    }

}

