<?php

namespace App\Http\Node;

use App\Http\Node\Model\TagsNodeModel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Validator;

class NewsNode extends BaseNode {

    public function __construct(Application $app, $model = null)
    {
        parent::__construct($app, $model);

        $this->fieldName([
            'id'         => 'ID',
            'title'      => 'Назва новини',
            'main_img'   => 'Фото',
            'short_body' => 'Короткий опис',
            'body'       => 'Текст',
            'tags'       =>  'Теги новини',
            'visible'    => 'Опублікована'
        ]);

        $this->setTypeField([
            'title'      => 'text',
            'main_img'   => ['file', 'upload'],
            'short_body' => 'textarea',
            'body'       => 'textarea',
            'tags'       => 'textarea',
            'visible'    => ['checkbox', 1],
        ]);

        $this->Tooltip([
            'tags' => 'Перерахуйте теги через кому'
        ]);

        $this->beforeInsert(function ($model)
        {
            $model->slug = str_slug(trim(Request::input('title')));
        });


        $this->afterInsert(function ($model)
        {
            $tags = explode(',', Request::input('tags'));

            foreach( $tags as $tag) {
                if(!empty( $tag)) {
                    $result = TagsNodeModel::firstOrCreate(
                        ['name' => $tag],
                        ['slug' => str_slug($tag)]);

                    $model->tags()->attach($result->id);

                }
            }
        });

        $this->afterUpdate(function ($model)
        {
            $tags = explode(',', Request::input('tags'));

            foreach( $tags as $tag) {
                if(!empty( $tag)) {
                    $result = TagsNodeModel::updateOrCreate(
                        ['name' => $tag],
                        ['slug' => str_slug($tag)]);
                }
            }

            $is_tags = TagsNodeModel::whereIn('name', $tags)->get();
            $sync_id = $is_tags->map(function ($el) {
                return $el->id;
            });

            $model->tags()->sync($sync_id);

        });

        $this->enableEditor(['body']);
        $this->enableEditor(['short_body']);

        $this->setTitle('Новини');
    }

    public function showDisplay ()
    {
        $this->fieldShow(['title', 'short_body', 'main_img', 'visible']);
       // $this->textLimit('short_body', 50);
      //  $this->textLimit('body', 50);

        $this->tableRowsRenderCollback(function ($row) {
            $row->main_img = view('renders.img-blog')->with(['img' => $row->main_img])->render();
            $row->visible = ($row->visible) ? 'Так' : 'Ні';
            return $row;
        });
    }

    public function showEditDisplay()
    {
        $this->fieldShow(['title', 'short_body', 'body', 'main_img', 'tags',
            'visible' ]);

         $this->Validation([
            'title' => 'required|string|max:255',
            'short_body' => 'required|string',
            'body' => 'required|string',
            'main_img' => 'nullable',
            'tags' => 'UniqueTag:'.Request::input('id'),        ]
        );

    }


    public function showInsertDisplay()
    {

        $this->showEditDisplay();
    }

    public function showDelete()
    {

    }



}
