<?php
/**
 * Created by PhpStorm.
 * User: vitalik
 * Date: 20.03.19
 * Time: 18:30
 */

namespace App\Http\Node;


use Illuminate\Foundation\Application;
use Trafik8787\LaraCrud\Contracts\NodeInterface;
use Trafik8787\LaraCrud\Models\NodeModelConfiguration;

abstract class BaseNode extends NodeModelConfiguration implements NodeInterface
{

    public function __construct(Application $app, $model = null)
    {
        parent::__construct($app, $model);

        $this->buttonApply(false);

        self::showDisplay();

        $this->alertDelete('Підтвердіть видалення єлементу');
    }

    public function showDisplay()
    {
        $this->buttonGroupDelete(false);
        $this->buttonCopy(false);
    }
}

