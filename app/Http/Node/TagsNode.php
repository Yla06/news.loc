<?php

namespace App\Http\Node;

use App\Http\Node\Model\TagsNodeModel;
use Illuminate\Contracts\Foundation\Application;

class TagsNode extends BaseNode
{

    public function __construct(Application $app, $model = null)
    {
        parent::__construct($app, $model);

    }

    public function showDisplay()
    {

    }

    public function showEditDisplay()
    {

    }

    public function showInsertDisplay()
    {

    }

    public function showDelete()
    {

    }

    public function hasTagInNews($tag_string)
    {
        $tags = collect(explode(',', $tag_string));

        $tags = $tags->map( function($tag) {
            return str_slug(trim($tag));
        });

        $news = TagsNodeModel::select( 'news.id','news.title','tags.name')
                            ->whereIn('tags.slug', $tags->toArray())
                            ->join('news_tags', 'news_tags.tags_id', '=', 'tags.id')
                            ->join('news', 'news.id', '=', 'news_tags.news_id')
                            ->get();

        //dd($news);
        $result_str = $news->map( function($item) {
            return "<span class='tag-news-error text-danger'>Тег  {$item->name} вже використовується в наступній новині  </span><a target='_blank'
                    href='". url("admin/news_node_model/{$item->id}/edit/ ") ."'>{$item->title}</a><br />";
        });

        $result_str = !$result_str->isEmpty() ?   implode('',$result_str->toArray()) : '';

        return response($result_str);
    }
}
