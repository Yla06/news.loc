<?php

namespace App\Http\Node;

use App\Http\Node\Model\CommentNodeModel;
use App\Http\Node\Model\NewsNodeModel;
use Illuminate\Contracts\Foundation\Application;

class CommentNode extends  BaseNode {

    public function __construct(Application $app, $model = null)
    {

        parent::__construct($app, $model);

        $this->fieldName([
            'news_id'    => 'Id новини',
            'name'       =>  'Ім\'я користувача',
            'surname'    =>  'Прізвище',
            'secondname' =>  'По-батькові',
            'phone'      =>  'Телефон',
            'coment'     =>  'Коментар',
            'status'    => 'Опублікувано'
        ]);

        $this->setTypeField([
            'news_id'     => ['select', [NewsNodeModel::class, 'id', 'id']],
            'coment'      => 'textarea',
            'status'      => ['radio', ['0' => 'Ні', '1' => 'На модерації', '2' => 'Так']],
        ]);

        $this->setTitle('Коментарі');

        $this->beforeInsert(function ($model){
            $model->name       = 'Админ';
            $model->parent_id  = '1';
        });

    }

    public function showDisplay ()
    {
        $this->fieldShow(['news_id', 'name', 'phone', 'coment', 'status']);

        //$this->columnColorWhere('status', '==', 2, 'lightyellow');

        $this->tableRowsRenderCollback(function ($row) {

            switch ($row->status) {
                case 0:
                    $row->status = "Не опубліковано";
                    break;
                case 1:
                    $row->status = "На модерації";
                    break;
                case 2:
                    $row->status = "Опубліковано";
                    break;
            }

            return $row;

        });

    }

    public function showEditDisplay()
    {
        $this->fieldShow(['news_id', 'status' ]);

//        $this->addViewsCustomTop(function ($model){
//
//            $comment = CommentNodeModel::find($model->id);
//
//            return view('renders.answer', compact('comment'));
//        });
    }


    public function showInsertDisplay()
    {
        $this->showEditDisplay();
    }

    public function showDelete()
    {

    }

}
