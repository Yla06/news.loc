<?php

namespace App\Http\Controllers;

use App\Http\Node\Model\CommentNodeModel;
use App\Http\Node\Model\NewsNodeModel;
use App\Http\Node\Model\TagsNodeModel;
use http\QueryString;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index( NewsNodeModel $newsNodeModel)
    {
        $news = $newsNodeModel->where('visible', 1)->orderBy('created_at', 'desc')->paginate(10);;

        return view('pages.home')->with(['news'=> $news]);
    }

    public function newsPage( NewsNodeModel $newsNodeModel, $slug, CommentNodeModel $commentNodeModel) {

        $expl = explode('-', $slug);
        $id = array_pop($expl);

        $post = $newsNodeModel->find($id);
        $comments = $post->coments()->get();

        $post->views = ++$post->views;
        $post->save();

        $tags = TagsNodeModel::join('news_tags', 'news_tags.tags_id', '=', 'tags.id')
                            ->join('news', 'news.id', '=', 'news_tags.news_id')
                            ->select('tags.name', 'news.id', 'news.slug')
                            ->where('news.id', '!=', $id)
                            ->where('news.visible', '=', '1')
                            ->get();

        $search = [];
        $replace = [];

        $tags->map(function($item) use(&$search, &$replace){
            $search[] = strtolower($item->name);
            $search[] = mb_convert_case(strtolower($item->name), MB_CASE_TITLE, "UTF-8");

            $replace[] = view('renders.link')->with(['item' => $item])->render();
            $replace[] = view('renders.link')->with(['item' => $item])->render();

        });

        $post->body = str_ireplace( $search, $replace, $post->body);

        return view('pages.news')->with(['post'=> $post, 'comments'=> $comments]);

    }

    public function addComment(Request $request, CommentNodeModel $commentNodeModel, $news_id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'secondname' => 'required|string|max:255',
            'phone' => 'required|max:255',
            'coment' => 'required|string|max:10000',
        ],
        [
            'name.required' => 'Поле Ім\'я є обов\'язковим для заповнення.',
            'surname.required' => 'Поле E-mail є обов\'язковим для заповнення.',
            'secondname.required' => 'Поле E-mail є обов\'язковим для заповнення.',
            'phone.required' => 'Поле Телефон є обов\'язковим для заповнення.',
            'coment.required' => 'Поле Телефон є обов\'язковим для заповнення.',
        ]
        );

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $commentNodeModel->name = $request->name;
        $commentNodeModel->surname = $request->surname;
        $commentNodeModel->secondname = $request->secondname;
        $commentNodeModel->phone = $request->phone;
        $commentNodeModel->coment = $request->coment;
        $commentNodeModel->news_id = $news_id;

        $commentNodeModel->save();

        return redirect()->back();
    }

    public function upStar($id, $num) {
        $news = NewsNodeModel::find($id);
        $news->star_all = $news->star_all + $num;
        $news->star_count = ++$news->star_count;

        if($news->star_count > 0) {
            $news->star = $news->star_all/$news->star_count;
        }

        $news->save();

        response()->json(['status' => 'OK']);

    }
}
