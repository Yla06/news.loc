const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js');

mix.combine([
   // 'resources/css/defoult_2.css',
   // 'resources/css/defoult.css',
    'node_modules/bootstrap/dist/css/bootstrap.css',
    'resources/css/style.css',
], 'public/css/all.css');
