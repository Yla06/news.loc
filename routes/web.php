<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/news/{id}', 'HomeController@newsPage')->name('news-page');

Route::post('add-coment/{news_id}', 'HomeController@addComment')->name('add-coment');

Route::get('up-star/{news_id}/{num}', 'HomeController@upStar')->name('upStar');

Route::group(['prefix' => config('lara-config.url_group'), 'middleware' => config('lara-config.middleware')], function () {

    Route::get('/tags/has-tag-in-news/{tag}', '\App\Http\Node\TagsNode@hasTagInNews')->name('tag-in-news');

});

